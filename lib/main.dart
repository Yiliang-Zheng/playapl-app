import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() => runApp(MyApp());

WebViewController controllerGlobal;

Future<bool> _exitApp(BuildContext context) async {
  if (await controllerGlobal.canGoBack()) {
    print("onwill goback");
    controllerGlobal.goBack();
  } else {
    return Future.value(false);
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//      title: 'Flutter Demo',
//      theme: ThemeData(
//        // This is the theme of your application.
//        //
//        // Try running your application with "flutter run". You'll see the
//        // application has a blue toolbar. Then, without quitting the app, try
//        // changing the primarySwatch below to Colors.green and then invoke
//        // "hot reload" (press "r" in the console where you ran "flutter run",
//        // or simply save your changes to "hot reload" in a Flutter IDE).
//        // Notice that the counter didn't reset back to zero; the application
//        // is not restarted.
//        primarySwatch: Colors.blue,
//      ),
      home: PlayAplWebView(),
    );
  }
}

class PlayAplWebView extends StatefulWidget {
  @override
  _PlayAplWebViewState createState() => _PlayAplWebViewState();
}

class _PlayAplWebViewState extends State<PlayAplWebView> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  Position _currentPosition;

  Future<Position> _getCurrentLocation() async {
    final Geolocator geoLocator = Geolocator();
    var result = await geoLocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    setState(() {
      _currentPosition = result;
    });
    return result;
  }

  @override
  void initState() {
    super.initState();
    final Geolocator geoLocator = Geolocator();
    geoLocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_controller.isCompleted) {
          var ctrl = await _controller.future.then((value) => value);
          var canGoBack = await ctrl.canGoBack();
          print(canGoBack);
          if (canGoBack) {
            ctrl.goBack();
            return Future.value(false);
          }
          return Future.value(true);
        } else {
          return Future.value(true);
        }
      },
      child: WebView(
        initialUrl: "https://playaplumbraco.fullhousegroup.com.au",
        javascriptMode: JavascriptMode.unrestricted,
        javascriptChannels: <JavascriptChannel>[
          JavascriptChannel(
              name: "GeoLocation",
              onMessageReceived: (JavascriptMessage msg) async {
                print("Outside if statement: $msg");
                if (msg.message == "current-location") {
                  print(msg.message);
                  var ctrl = await this._controller.future;
                  var position = await this._getCurrentLocation();
                  if (position != null) {
                    ctrl.evaluateJavascript(
                        "GlobalAccess.onUpdateCurrentLocation(${position.latitude},${position.longitude})").then((result){
                          print(result);
                    });
                  }
                }
              }),
        ].toSet(),
        onWebViewCreated: (WebViewController webViewController) {
          this._controller.complete(webViewController);
        },
        onPageStarted: (String url) {
          print("Page Start $url");
        },
        onPageFinished: (String url) {
          _controller.future.then((ctrl) {
            if (this._currentPosition != null) {
              print(this._currentPosition);
              ctrl
                  .evaluateJavascript(
                      "window.onReceiveCoords(${_currentPosition.latitude}, ${_currentPosition.longitude});")
                  .then((result) {
                print(result);
              });
            }
          });
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
